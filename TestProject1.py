import pandas as pd
import numpy as np
#from datetime import datetime
import datetime
from io import StringIO
import pickle
import catboost as cb
import lightgbm as lg

pd.options.display.max_columns = None
pd.options.display.max_rows = None



import json

from flask import Flask, request,jsonify
from flask_cors import CORS

ui_event_sever = Flask("UI Event Server")
CORS(ui_event_sever)



@ui_event_sever.route("/TestProject",
                      methods=["POST"])
def PredictRPM():
    """
    Just a dummy route to check if the server is working.
    :return response: String
    """

    response_dict = {}
    if request.method == "POST":
        context_action = json.loads(request.data)
        

        predict_df= pd.DataFrame()
        Mile = context_action['Miles']
        Rate = context_action['Rate']
        RPM = Rate / Mile
        
        
        
        response_dict["Final_RPM"]=RPM
    
    return jsonify(response_dict)


ui_event_sever.run(host = '0.0.0.0')
